#include "CollisionPrimitive.h"

CollisionPrimitive::CollisionPrimitive(){
	XMMATRIX xmOffSet = XMMatrixIdentity();
	XMStoreFloat4x4(&mOffset, xmOffSet);
}

CollisionPrimitive::~CollisionPrimitive(){

}

void CollisionPrimitive::CalculateInternals() {
	mBody->CalculateTransformMatrix();

	XMMATRIX xmTransform = XMLoadFloat4x4(&mBody->GetTransformMatrix());
	XMMATRIX xmOffset = XMLoadFloat4x4(&mOffset);
	xmTransform *= xmOffset;
}

XMFLOAT3 CollisionPrimitive::GetAxis(unsigned int i) const {
	assert(mBody != nullptr);

	XMFLOAT3 axis;

	const auto &bodyTransformMatrix = mBody->GetTransformMatrix();
	
	if (i == 0) {
		axis.x = bodyTransformMatrix._11;
		axis.y = bodyTransformMatrix._12;
		axis.z = bodyTransformMatrix._13;
	}
	else if (i == 1) {
		axis.x = bodyTransformMatrix._21;
		axis.y = bodyTransformMatrix._22;
		axis.z = bodyTransformMatrix._23;
	}
	else if (i == 2) {
		axis.x = bodyTransformMatrix._31;
		axis.y = bodyTransformMatrix._32;
		axis.z = bodyTransformMatrix._33;
	}
	else if (i == 3) {
		axis.x = bodyTransformMatrix._41;
		axis.y = bodyTransformMatrix._42;
		axis.z = bodyTransformMatrix._43;
	}
	else {
		axis = XMFLOAT3(0.0f, 0.0f, 0.0f);
	}

	return axis;
}