#ifndef _COLLISIONCUBE_H_
#define _COLLISIONCUBE_H_

//Includes
#include "CollisionPrimitive.h"

class CollisionCube : public CollisionPrimitive{
public:
	XMFLOAT3 mHalfSize;

public:
	CollisionCube();
	~CollisionCube();
};

#endif