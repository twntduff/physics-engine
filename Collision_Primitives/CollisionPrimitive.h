#ifndef _COLLISIONPRIMITIVE_H_
#define _COLLISIONPRIMITIVE_H_

//Includes
#include "PhysicsComponent.h"

class CollisionPrimitive
{
public:
	PhysicsComponent* mBody;
	XMFLOAT4X4 mOffset;

public:
	CollisionPrimitive();
	virtual ~CollisionPrimitive();

	void CalculateInternals();
	XMFLOAT3 GetAxis(unsigned int i) const;
};

#endif