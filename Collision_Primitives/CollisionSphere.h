#ifndef _COLLISIONSPHERE_H_
#define _COLLISIONSPHERE_H_

//Includes
#include "CollisionPrimitive.h"

class CollisionSphere : public CollisionPrimitive
{
public:
	float mRadius;

public:
	CollisionSphere();
	~CollisionSphere();
};

#endif