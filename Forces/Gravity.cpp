#include "Gravity.h"

Gravity::Gravity(){
}

Gravity::~Gravity(){
}

bool Gravity::Initialize(PhysicsComponent* entity, const float velX, const float velY, const float velZ) {
	mGravity = XMFLOAT3(velX, velY, velZ);
	entity->SetAwake(true);

	return true;
}

void Gravity::UpdateForce(PhysicsComponent* entity, float dt) {
	if (entity->HasInfiniteMass()) {
		return;
	}

	entity->SetAwake(true);

	const float mass = entity->GetMass();

	entity->AddForce(XMFLOAT3(mGravity.x * mass, mGravity.y * mass, mGravity.z * mass));
}