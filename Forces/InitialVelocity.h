#ifndef _INTITIALVELOCITY_H_
#define _INTITIALVELOCITY_H_

//Includes
#include "Force.h"

class InitialVelocity : public Force
{
public:
	InitialVelocity();
	~InitialVelocity();

	bool Initialize(PhysicsComponent* entity, const float velX, const float velY, const float velZ);
	void UpdateForce(PhysicsComponent* entity, float dt);
};

#endif