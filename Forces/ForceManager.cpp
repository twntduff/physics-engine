#include "ForceManager.h"
#include "Gravity.h"
#include "InitialVelocity.h"
#include "Spring.h"

ForceManager::ForceManager(){

}

ForceManager::ForceManager(const ForceManager& other) {
	
}

ForceManager::~ForceManager(){

}

void ForceManager::DeleteForce(PhysicsComponent* entity, Force* force) {

}

void ForceManager::ClearForces() {
	
}

void ForceManager::Update(float dt) {
	vector<ForceDecl>::const_iterator it = mDeclerations.begin();
	while (it != mDeclerations.end()) {
		it->ForceGen->UpdateForce(it->Entity, dt);
		it++;
	}
}

bool ForceManager::AddSpring(PhysicsComponent* entity, const XMFLOAT3& connectionPoint, PhysicsComponent* other, const XMFLOAT3& otherConnectionPoint, float restLength, float springConstant) {
	if (!entity || entity->HasInfiniteMass()) {
		return false;
	}

	Spring* force = new Spring;

	if (!force->Initialize(connectionPoint, otherConnectionPoint, other, springConstant, restLength)) {
		return false;
	}

	ForceDecl decl;
	decl.Entity = entity;
	decl.ForceGen.reset(force);
	mDeclerations.push_back(move(decl));

	return true;
}

bool ForceManager::AddGravity(PhysicsComponent* entity, const float velX, const float velY, const float velZ) {
	if (!entity || entity->HasInfiniteMass()) {
		return false;
	}

	Gravity* force = new Gravity;
	
	if (!force->Initialize(entity, velX, velY, velZ)) {
		return false;
	}

	ForceDecl decl;
	decl.Entity = entity;
	decl.ForceGen.reset(force);
	mDeclerations.push_back(move(decl));

	return true;
}

bool ForceManager::AddInitialVelocity(PhysicsComponent* entity, const float velX, const float velY, const float velZ) {
	if (!entity || entity->HasInfiniteMass()) {
		return false;
	}

	InitialVelocity* force = new InitialVelocity;

	if (!force->Initialize(entity, velX, velY, velZ)) {
		return false;
	}

	ForceDecl decl;
	decl.Entity = entity;
	decl.ForceGen.reset(force);
	mDeclerations.push_back(move(decl));

	return true;
}

vector<ForceDecl>& ForceManager::GetForceDeclerations() {
	return mDeclerations;
}