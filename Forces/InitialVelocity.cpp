#include "InitialVelocity.h"

InitialVelocity::InitialVelocity(){

}

InitialVelocity::~InitialVelocity(){

}

bool InitialVelocity::Initialize(PhysicsComponent* entity, const float velX, const float velY, const float velZ) {
	entity->SetAwake(true);
	entity->AddVelocity(XMFLOAT3(velX, velY, velZ));

	return true;
}

void InitialVelocity::UpdateForce(PhysicsComponent* entity, float dt) {
	//entity->AddForce(XMFLOAT3(mVelocity.x, mVelocity.y, mVelocity.z));
	
}