#ifndef _GRAVITY_H_
#define _GRAVITY_H_

//Includes
#include "Force.h"

class Gravity : public Force
{
private:
	XMFLOAT3 mGravity;

public:
	Gravity();
	~Gravity();

	bool Initialize(PhysicsComponent* entity, const float velX, const float velY, const float velZ);
	void UpdateForce(PhysicsComponent* entity, float dt);
};

#endif