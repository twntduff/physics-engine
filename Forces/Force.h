#ifndef _FORCE_H_
#define _FORCE_H_

//Includes
#include "PhysicsComponent.h"

class Force
{
public:
	Force();
	~Force();

	virtual void UpdateForce(PhysicsComponent* entity, float dt) = 0;
};

#endif