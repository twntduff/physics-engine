#ifndef _FORCEMANAGER_H_
#define _FORCEMANAGER_H_

//Includes
#include "Force.h"
#include <vector>

class Gravity;
class InitialVelocity;

using namespace std;

struct ForceDecl{
		PhysicsComponent* Entity;
		unique_ptr<Force> ForceGen;
	};

class ForceManager
{
protected:
	vector<ForceDecl> mDeclerations;

public:
	ForceManager();
	ForceManager(const ForceManager& other);
	~ForceManager();

	void DeleteForce(PhysicsComponent* entity, Force* force);
	void ClearForces();
	void Update(float dt);

	bool AddSpring(PhysicsComponent* entity, const XMFLOAT3& connectionPoint, PhysicsComponent* other, const XMFLOAT3& otherConnectionPoint, float restLength, float springConstant);
	bool AddGravity(PhysicsComponent* entity, const float velX, const float velY, const float velZ);
	bool AddInitialVelocity(PhysicsComponent* entity, const float velX, const float velY, const float velZ);

	vector<ForceDecl>& GetForceDeclerations();
};

#endif