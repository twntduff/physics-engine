#include "CollisionDetector.h"
#include "CollisionCube.h"
#include "CollisionSphere.h"

#define MAX3(a,b,c) ((a)>(b) ? ((a)>(c) ? (a) : (c)) : ((b)>(c) ? (b) : (c)))
#define CLAMP(n,nummin,nummax) ((n)<(nummin) ? (nummin):((n)>(nummax) ?(nummax):(n)))
#define CLAMPVEC3_SCALAR(n,nummin,nummax) (XMFLOAT3(CLAMP((n).x,(nummin),(nummax)),CLAMP((n).y,(nummin),(nummax)),CLAMP((n).z,(nummin),(nummax))))
#define CLAMPVEC3(n,nummin,nummax) (XMFLOAT3(CLAMP((n).x,(nummin).x,(nummax).x),CLAMP((n).y,(nummin).y,(nummax).y),CLAMP((n).z,(nummin).z,(nummax).z)))

bool CollisionDetector::SphereAndSphere(const CollisionSphere& sphere1, const CollisionSphere& sphere2, CollisionData* data) {
	if (!data->HasMoreContacts()) return false;

	//Calculate the vector between the spheres
	const XMVECTOR xmMidline = XMLoadFloat3(&sphere1.GetAxis(3)) - XMLoadFloat3(&sphere2.GetAxis(3));

	//Calculate the magnitude of the vector between spheres
	const float size = XMVector3Length(xmMidline).m128_f32[0];

	//Check if size is between 0 and the sum of both radi
	if (size <= 0.0f || size >= sphere1.mRadius + sphere2.mRadius) {
		return false;
	}

	//Calculate normal
	const XMVECTOR xmNormal = xmMidline * (1.0f / size);
	
	//Store contact data
	Contact* contact = data->AddContact();
	contact->SetBodiesData(sphere1.mBody, sphere2.mBody);
	
	XMStoreFloat3(&contact->mContactNormal, xmNormal);
	XMStoreFloat3(&contact->mContactPoint, XMLoadFloat3(&sphere1.GetAxis(3)) - xmMidline * 0.5f);
	contact->mPenetration = sphere1.mRadius + sphere2.mRadius - size;

	return true;
}

static inline float TransformToAxis(const CollisionCube& cube, const XMFLOAT3& axis) {
	return (cube.mHalfSize.x * fabs(XMVector3Dot(XMLoadFloat3(&axis), XMVector3Normalize(XMLoadFloat3(&cube.GetAxis(0)))).m128_f32[0]) +
			cube.mHalfSize.y * fabs(XMVector3Dot(XMLoadFloat3(&axis), XMVector3Normalize(XMLoadFloat3(&cube.GetAxis(1)))).m128_f32[0]) +
			cube.mHalfSize.z * fabs(XMVector3Dot(XMLoadFloat3(&axis), XMVector3Normalize(XMLoadFloat3(&cube.GetAxis(2)))).m128_f32[0]));
}

static inline float PenetrationOnAxis(const CollisionCube& cube1, const CollisionCube& cube2, const XMFLOAT3& axis, const XMFLOAT3& toCentre) {
	//Project halfsize on axis
	const float oneProject = TransformToAxis(cube1, axis);
	const float twoProject = TransformToAxis(cube2, axis);

	//Project this onto axis
	const float distance = fabs(XMVector3Dot(XMLoadFloat3(&toCentre), XMLoadFloat3(&axis)).m128_f32[0]);

	return (oneProject + twoProject - distance);
}

static inline bool TryAxis(const CollisionCube& cube1, const CollisionCube& cube2, XMFLOAT3 axis, const XMFLOAT3& toCentre, unsigned int index, float& smallestPenetration, unsigned int& smallestCase) {
	const float squaredLength = ((axis.x * axis.x) + (axis.y * axis.y) + (axis.z * axis.z));
	
	//Do not check almost parallel axes
	if (squaredLength < 0.00001f) {
		return true;
	}

	//Make sure axis is normalized
	XMFLOAT3 normalizedAxis;
	XMStoreFloat3(&normalizedAxis, XMVector3Normalize(XMLoadFloat3(&axis)));

	const float penetration = PenetrationOnAxis(cube1, cube2, normalizedAxis, toCentre);

	if (penetration < 0.0f) {
		return false;
	}

	if (penetration < smallestPenetration) {
		smallestPenetration = penetration;
		smallestCase = index;
	}

	return true;
}

static inline XMFLOAT3 ContactPoint(const XMFLOAT3& pOne, const XMFLOAT3& dOne, float oneSize, const XMFLOAT3& pTwo, const XMFLOAT3& dTwo, float twoSize, bool useOne) {
	XMFLOAT3 toSt, cOne, cTwo;

	const float smOne = (dOne.x * dOne.x) + (dOne.y * dOne.y) + (dOne.z * dOne.z);
	const float smTwo = (dTwo.x * dTwo.x) + (dTwo.y * dTwo.y) + (dTwo.z * dTwo.z);
	const float dpOneTwo = XMVector3Dot(XMLoadFloat3(&dOne), XMLoadFloat3(&dTwo)).m128_f32[0];

	XMStoreFloat3(&toSt, XMLoadFloat3(&pOne) - XMLoadFloat3(&pTwo));
	const float dpStaOne = XMVector3Dot(XMLoadFloat3(&dOne), XMLoadFloat3(&toSt)).m128_f32[0];
	const float dpStaTwo = XMVector3Dot(XMLoadFloat3(&dTwo), XMLoadFloat3(&toSt)).m128_f32[0];

	const float denom = smOne * smTwo - dpOneTwo * dpOneTwo;


	//Zero denom indicates parallel lines
	if (fabs(denom) < 0.0001f) {
		return useOne ? pOne : pTwo;
	}

	const float mua = (dpOneTwo * dpStaTwo - smTwo * dpStaOne) / denom;
	const float mub = (smOne * dpStaTwo - dpOneTwo * dpStaOne) / denom;

	//If any edge has the nearest point out of bounds,
	//then the edges are not crossed.
	//There is an edge face contact.
	if (mua > oneSize ||
		mua < -oneSize ||
		mub > twoSize ||
		mub < -twoSize) {
		return useOne ? pOne : pTwo;
	}
	else {
		XMStoreFloat3(&cOne, XMLoadFloat3(&pOne) + XMLoadFloat3(&dOne) * mua);
		XMStoreFloat3(&cTwo, XMLoadFloat3(&pTwo) + XMLoadFloat3(&dTwo) * mub);

		XMFLOAT3 contactPoint;
		XMStoreFloat3(&contactPoint, (XMLoadFloat3(&cOne) * 0.5f) + (XMLoadFloat3(&cTwo) * 0.5f));

		return contactPoint;
	}
}

void FillPointFaceCubeCube(const CollisionCube& cube1, const CollisionCube& cube2, const XMFLOAT3& toCentre, CollisionData* data, unsigned int best, float penetration) {
	//Work out which of the two faces is colliding with axis
	XMFLOAT3 normal;
	XMStoreFloat3(&normal, XMVector3Normalize(XMLoadFloat3(&cube1.GetAxis(best))));
	const float distance = XMVector3Dot(XMLoadFloat3(&cube1.GetAxis(best)), XMLoadFloat3(&toCentre)).m128_f32[0];

	if (distance > 0) {
		XMStoreFloat3(&normal, XMLoadFloat3(&normal) * -1.0f);
	}

	//Work out which vertex of cube 2 is involved in collision.
	//Using toCentre does not work!
	XMFLOAT3 vertex = cube2.mHalfSize;

	if (XMVector3Dot(XMLoadFloat3(&cube2.GetAxis(0)), XMLoadFloat3(&normal)).m128_f32[0] < 0.0f) {
		vertex.x = -vertex.x;
	}
	if (XMVector3Dot(XMLoadFloat3(&cube2.GetAxis(1)), XMLoadFloat3(&normal)).m128_f32[0] < 0.0f) {
		vertex.y = -vertex.y;
	}
	if (XMVector3Dot(XMLoadFloat3(&cube2.GetAxis(2)), XMLoadFloat3(&normal)).m128_f32[0] < 0.0f) {
		vertex.z = -vertex.z;
	}

	Contact* contact = data->AddContact();
	contact->SetBodiesData(cube1.mBody, cube2.mBody);
	XMStoreFloat3(&contact->mContactNormal, XMLoadFloat3(&normal));
	contact->mPenetration = penetration;
	XMStoreFloat3(&contact->mContactPoint, XMVector3Transform(XMLoadFloat3(&vertex), XMLoadFloat4x4(&cube2.mBody->GetTransformMatrix())));
}

#define CHECK_OVERLAP(axis, index) \
    if (!TryAxis(cube1, cube2, (axis), toCentre, (index), pen, best)) return false;

bool CollisionDetector::CubeAndCube(const CollisionCube& cube1, const CollisionCube& cube2, CollisionData* data) {
	if (!data->HasMoreContacts()) return false;

	//Find vector between both centres
	XMFLOAT3 toCentre;
 	XMStoreFloat3(&toCentre, XMLoadFloat3(&cube2.GetAxis(3)) - XMLoadFloat3(&cube1.GetAxis(3)));
	
	//Start by assuming there is no contact;
	float pen = FLT_MAX;
	unsigned int best = 15;

	//Check each axes and return if there is a seperating axis.
	//Keep track of the axis with the smallest penetration.
	CHECK_OVERLAP(cube1.GetAxis(0), 0);
	CHECK_OVERLAP(cube1.GetAxis(1), 1);
	CHECK_OVERLAP(cube1.GetAxis(2), 2);

	CHECK_OVERLAP(cube2.GetAxis(0), 3);
	CHECK_OVERLAP(cube2.GetAxis(1), 4);
	CHECK_OVERLAP(cube2.GetAxis(2), 5);

	//Store best axis-major in case we run into almost
	//parallel edge collision later
	const unsigned int bestSingleAxis = best;
	
	XMFLOAT3 crossAxes;
	XMStoreFloat3(&crossAxes, XMVector3Cross(XMLoadFloat3(&cube1.GetAxis(0)), XMLoadFloat3(&cube2.GetAxis(0))));
	CHECK_OVERLAP(crossAxes, 6);
	XMStoreFloat3(&crossAxes, XMVector3Cross(XMLoadFloat3(&cube1.GetAxis(0)), XMLoadFloat3(&cube2.GetAxis(1))));
	CHECK_OVERLAP(crossAxes, 7);
	XMStoreFloat3(&crossAxes, XMVector3Cross(XMLoadFloat3(&cube1.GetAxis(0)), XMLoadFloat3(&cube2.GetAxis(2))));
	CHECK_OVERLAP(crossAxes, 8);
	XMStoreFloat3(&crossAxes, XMVector3Cross(XMLoadFloat3(&cube1.GetAxis(1)), XMLoadFloat3(&cube2.GetAxis(0))));
	CHECK_OVERLAP(crossAxes, 9);
	XMStoreFloat3(&crossAxes, XMVector3Cross(XMLoadFloat3(&cube1.GetAxis(1)), XMLoadFloat3(&cube2.GetAxis(1))));
	CHECK_OVERLAP(crossAxes, 10);
	XMStoreFloat3(&crossAxes, XMVector3Cross(XMLoadFloat3(&cube1.GetAxis(1)), XMLoadFloat3(&cube2.GetAxis(2))));
	CHECK_OVERLAP(crossAxes, 11);
	XMStoreFloat3(&crossAxes, XMVector3Cross(XMLoadFloat3(&cube1.GetAxis(2)), XMLoadFloat3(&cube2.GetAxis(0))));
	CHECK_OVERLAP(crossAxes, 12);
	XMStoreFloat3(&crossAxes, XMVector3Cross(XMLoadFloat3(&cube1.GetAxis(2)), XMLoadFloat3(&cube2.GetAxis(1))));
	CHECK_OVERLAP(crossAxes, 13);
	XMStoreFloat3(&crossAxes, XMVector3Cross(XMLoadFloat3(&cube1.GetAxis(2)), XMLoadFloat3(&cube2.GetAxis(2))));
	CHECK_OVERLAP(crossAxes, 14);

	//Make sure we have a result
	assert(best < 15);

	//Collision confirmed
	//We know which axis has the smallest penetration.
	//Deal with differently depending on case
	if (best < 3) {
		//We have a vertex of cube 2 on a face of cube 1
		FillPointFaceCubeCube(cube1, cube2, toCentre, data, best, pen);
		return true;
	}
	else if (best < 6) {
		//We have a vertex of cube 1 on face of cube 2.
		FillPointFaceCubeCube(cube2, cube1, XMFLOAT3(toCentre.x * -1.0f, toCentre.y * -1.0f, toCentre.z * -1.0f), data, best - 3, pen);
		return true;
	}
	else {
		//We have an edge contact
		//Find which axes
		best -= 6;
		unsigned int oneAxisIndex = best / 3;
		unsigned int twoAxisIndex = best % 3;
		XMFLOAT3 oneAxis = cube1.GetAxis(oneAxisIndex);
		XMFLOAT3 twoAxis = cube2.GetAxis(twoAxisIndex);
		XMVECTOR xmAxis = XMVector3Normalize(XMVector3Cross(XMLoadFloat3(&oneAxis), XMLoadFloat3(&twoAxis)));

		//This axis should point from cube 1 to cube 2
		if (XMVector3Dot(xmAxis, XMLoadFloat3(&toCentre)).m128_f32[0] > 0) {
			xmAxis *= -1.0f;
		}

		//We have the axis but not the edges.
		//Find which of the 4 parallel edges for each object
		XMVECTOR xmPtOnOneEdge = XMLoadFloat3(&cube1.mHalfSize);
		XMVECTOR xmPtOnTwoEdge = XMLoadFloat3(&cube2.mHalfSize);
		for (unsigned int i = 0; i < 3; i++) {
			if (i == oneAxisIndex) {
				xmPtOnOneEdge.m128_f32[i] = 0.0f;
			}
			else if (XMVector3Dot(XMLoadFloat3(&cube1.GetAxis(i)), xmAxis).m128_f32[0] > 0) {
				xmPtOnOneEdge.m128_f32[i] = -xmPtOnOneEdge.m128_f32[i];
			}

			if (i == twoAxisIndex) {
				xmPtOnTwoEdge.m128_f32[i] = 0.0f;
			}
			else if (XMVector3Dot(XMLoadFloat3(&cube2.GetAxis(i)), xmAxis).m128_f32[0] < 0) {
				xmPtOnTwoEdge.m128_f32[i] = -xmPtOnTwoEdge.m128_f32[i];
			}
		}

		//Move them into world coords
		xmPtOnOneEdge = XMVector3Transform(xmPtOnOneEdge, XMLoadFloat4x4(&cube1.mBody->GetTransformMatrix()));
		xmPtOnTwoEdge = XMVector3Transform(xmPtOnTwoEdge, XMLoadFloat4x4(&cube2.mBody->GetTransformMatrix()));

		//We need to find out point of closest approach of the two line segs
		XMFLOAT3 ptOnOneEdge, ptOnTwoEdge;
		XMStoreFloat3(&ptOnOneEdge, xmPtOnOneEdge);
		XMStoreFloat3(&ptOnTwoEdge, xmPtOnTwoEdge);
		const XMVECTOR xmOneHalfWidth = XMLoadFloat3(&cube1.mHalfSize);
		const XMVECTOR xmTwoHalfWidth = XMLoadFloat3(&cube2.mHalfSize);
		const XMFLOAT3 vertex = ContactPoint(ptOnOneEdge, oneAxis, xmOneHalfWidth.m128_f32[oneAxisIndex],
									ptOnTwoEdge, twoAxis, xmTwoHalfWidth.m128_f32[twoAxisIndex],
									bestSingleAxis > 2);

		//Store contact data
		Contact* contact = data->AddContact();
		contact->mPenetration = pen;
		XMStoreFloat3(&contact->mContactNormal, xmAxis);
		contact->mContactPoint = vertex;
		contact->SetBodiesData(cube1.mBody, cube2.mBody);

		return true;
	}

	return false;
}

#undef CHECK_OVERLAP

bool CollisionDetector::SphereAndCube(const CollisionSphere& sphere, const CollisionCube& cube, CollisionData* data) {
	if (!data->HasMoreContacts()) return false;

	//Get sphere center in world coordinates
	const XMVECTOR xmPositionSphere_world = XMLoadFloat3(&sphere.GetAxis(3));
	//Get cube center in world coordinates
	const XMVECTOR xmPositionCube_world = XMLoadFloat3(&cube.GetAxis(3));

	//Transform sphere center into cube coordinates
	const XMVECTOR xmrelCenter = XMVector3Transform(xmPositionSphere_world, XMMatrixInverse(nullptr, XMLoadFloat4x4(&cube.mBody->GetTransformMatrix())));
	XMFLOAT3 relCenter; XMStoreFloat3(&relCenter, xmrelCenter);

	//EARLY CHECK: Check if sphere is outside of cube and return if true
	if (abs(relCenter.x) - sphere.mRadius > cube.mHalfSize.x ||
		abs(relCenter.y) - sphere.mRadius > cube.mHalfSize.y ||
		abs(relCenter.z) - sphere.mRadius > cube.mHalfSize.z) {
		return false;
	}

	//Find closest point on cube
	const XMVECTOR closestPointOnBox_boxSpace = XMLoadFloat3(&CLAMPVEC3(relCenter, XMFLOAT3(-cube.mHalfSize.x,-cube.mHalfSize.y,-cube.mHalfSize.z), cube.mHalfSize));
	//Calculate distance from closest point on cube to sphere centre
	const float distance = XMVector3Length(closestPointOnBox_boxSpace - xmrelCenter).m128_f32[0];

	//Check for contact
	if (distance > sphere.mRadius) return false;

	//Create contact object
	Contact* contact = data->AddContact();
	contact->SetBodiesData(cube.mBody, sphere.mBody);

	//Transform closest point on cube into world coordinates
	const XMVECTOR closestPointOnBox_world = XMVector3Transform(closestPointOnBox_boxSpace, XMLoadFloat4x4(&cube.mBody->GetTransformMatrix()));
	//Calculate contact normal
	const XMVECTOR xmContactNormal = XMVector3Normalize(closestPointOnBox_world - xmPositionSphere_world);

	//Store contact data
	XMStoreFloat3(&contact->mContactNormal, xmContactNormal);
	XMStoreFloat3(&contact->mContactPoint, closestPointOnBox_world);
	contact->mPenetration = sphere.mRadius - distance;

	return true;
}