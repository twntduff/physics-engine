#ifndef _COLLISIONDETECTOR_H_
#define _COLLISIONDETECTOR_H_

#include "Contact.h"

#include <vector>

class CollisionSphere;
class CollisionCube;

struct CollisionData {
	std::vector<Contact> mContacts;

	unsigned int mContactCount;
	float mTolerance;

	void Reset(unsigned int maxContacts) {
		mContactCount = 0;
		mContacts.resize(maxContacts);
	}

	bool HasMoreContacts() const {
		return mContactCount < mContacts.size();
	}

	Contact* AddContact() {
		return &mContacts[mContactCount++];
	}
};

class CollisionDetector
{
public:
	static bool SphereAndSphere(const CollisionSphere& sphere1, const CollisionSphere& sphere2, CollisionData* data);
	static bool CubeAndCube(const CollisionCube& cube1, const CollisionCube& cube2, CollisionData* data);
	static bool SphereAndCube(const CollisionSphere& sphere, const CollisionCube& cube, CollisionData* data);
};

#endif