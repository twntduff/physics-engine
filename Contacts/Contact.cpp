#include "Contact.h"

#include <algorithm>
inline float lerp(float a, float b, float t) { return a*(1.0f - t) + b*(t); }

Contact::Contact(){
	mBodies[0] = nullptr;
	mBodies[1] = nullptr;
	mFriction = 0.0f;
	mRestitution = 0.0f;
	mPenetration = 0.0f;
	mDesiredDeltaVelocity = 0.0f;
	mRelativeContactPosition[0] = XMFLOAT3(0.0f, 0.0f, 0.0f);
	mRelativeContactPosition[1] = XMFLOAT3(0.0f, 0.0f, 0.0f);
	mContactNormal = XMFLOAT3(0.0f, 0.0f, 0.0f);
	mContactPoint = XMFLOAT3(0.0f, 0.0f, 0.0f);
	mContactVelocity = XMFLOAT3(0.0f, 0.0f, 0.0f);
	XMStoreFloat3x3(&mContactToWorld, XMMatrixIdentity());
}

Contact::~Contact(){

}

float Contact::GetDesiredDeltaVelocity() {
	return mDesiredDeltaVelocity;
}

void Contact::SetBodiesData(PhysicsComponent* bodyOne, PhysicsComponent* bodyTwo) {
	mBodies[0] = bodyOne;
	mBodies[1] = bodyTwo;

	//Get lowest restitution coefficient
	const float restitutionLow = (std::min)(bodyOne->GetRestitution(), bodyTwo->GetRestitution());
	//Get highest restitution coefficient
	const float restitutionHigh = (std::max)(bodyOne->GetRestitution(), bodyTwo->GetRestitution());

	//Get lowest friction coefficient
	const float frictionLow = (std::min)(bodyOne->GetFriction(), bodyTwo->GetFriction());
	//Get highest friction coefficient 
	const float frictionHigh = (std::max)(bodyOne->GetFriction(), bodyTwo->GetFriction());

	//Linear interpolate between both values
	mRestitution = lerp(restitutionLow, restitutionHigh, 0.8f);
	//Linear interpolate between both values
	mFriction = lerp(frictionLow, frictionHigh, 0.15f);
}

void Contact::MatchAwakeState() {
	//If body1 is NULL return
	if (!mBodies[1]) {
		return;
	}

	//Get Awake state for both bodies
	bool body0Awake = mBodies[0]->GetAwake();
	bool body1Awake = mBodies[1]->GetAwake();

	//XOR between both awake states
	if (body0Awake ^ body1Awake) {
		if (body0Awake) {
			//Set body1 to awake
			mBodies[1]->SetAwake(true);
		}
		else {
			//Set body2 to awake
			mBodies[0]->SetAwake(true);
		}
	}
}

void Contact::SwapBodies() {
	//Reverse contact normal
	XMStoreFloat3(&mContactNormal, XMLoadFloat3(&mContactNormal) * -1.0f);

	//Swap bodies
	PhysicsComponent* temp = mBodies[0];
	mBodies[0] = mBodies[1];
	mBodies[1] = temp;
}

void Contact::CalculateContactBasis() {
	XMFLOAT3 contactTangent[2];
	
	//Check whether the z-axis is closer to the y or x
	if (fabs(mContactNormal.x) > fabs(mContactNormal.y)) {
		//Ensure the results are normalized
		const float s = 1.0f / sqrtf(mContactNormal.z * mContactNormal.z + mContactNormal.x * mContactNormal.x);

		//x-axis is at right angle to world y-axis
		contactTangent[0].x = mContactNormal.z * s;
		contactTangent[0].y = 0.0f;
		contactTangent[0].z = -mContactNormal.x * s;

		//y-axis is at right angle to x-axis and z-axis
		contactTangent[1].x = mContactNormal.y * contactTangent[0].x;
		contactTangent[1].y = mContactNormal.z * contactTangent[0].x - mContactNormal.x * contactTangent[0].z;
		contactTangent[1].z = -mContactNormal.y * contactTangent[0].x;
	}
	else{
		//Ensure results are normalized
		const float s = 1.0f / sqrtf(mContactNormal.z * mContactNormal.z + mContactNormal.y * mContactNormal.y);

		//x-axis is at right angle to world x-axis
		contactTangent[0].x = 0;
		contactTangent[0].y = -mContactNormal.z * s;
		contactTangent[0].z = mContactNormal.y * s;

		//y-axis is at right angle to x-axis and z-axis
		contactTangent[1].x = mContactNormal.y * contactTangent[0].z - mContactNormal.z * contactTangent[0].y;
		contactTangent[1].y = -mContactNormal.x * contactTangent[0].z;
		contactTangent[1].z = mContactNormal.x * contactTangent[0].y;
	}

	//Store contact world data
	mContactToWorld._11 = mContactNormal.x;
	mContactToWorld._21 = mContactNormal.y;
	mContactToWorld._31 = mContactNormal.z;

	mContactToWorld._12 = contactTangent[0].x;
	mContactToWorld._22 = contactTangent[0].y;
	mContactToWorld._32 = contactTangent[0].z;

	mContactToWorld._13 = contactTangent[1].x;
	mContactToWorld._23 = contactTangent[1].y;
	mContactToWorld._33 = contactTangent[1].z;
}

XMFLOAT3 Contact::CalculateLocalVelocity(unsigned int bodyIndex, float dt) {
	PhysicsComponent* body = mBodies[bodyIndex];

	//Work out the velocity at the contact point.
	const XMVECTOR velocityAngular = XMVector3Cross(XMLoadFloat3(&body->GetRotation()), XMLoadFloat3(&mRelativeContactPosition[bodyIndex]));
	const XMVECTOR velocityLinear = XMLoadFloat3(&body->GetVelocity());
	//Calculate velocity without reaction forces
	const XMVECTOR acceleration = XMLoadFloat3(&body->GetLastFrameAcceleration()) * dt;

	//Add planar velocities. Will be removed if there is enough friction in velocity resolution.
	//Calculate velocity in contact coordinates.
	const XMVECTOR localVelocity_world = velocityAngular + velocityLinear - (XMLoadFloat3(&mContactNormal) * XMVector3Dot(acceleration,XMLoadFloat3(&mContactNormal)).m128_f32[0]);
	XMFLOAT3 localVelocity_contact;
	XMStoreFloat3(&localVelocity_contact, XMVector3Transform(localVelocity_world, XMLoadFloat3x3(&mContactToWorld)));

	return localVelocity_contact;
}

void Contact::CalculateDesiredDeltaVelocity(float dt) {
	//Set velocity limit
	const float velocityLimit = 0.25f;
	float velocityFromAcc = 0.0f;
	XMVECTOR scaledLFA;

	//Check if body0 is awake
	if (mBodies[0]->GetAwake()) {
		//Scale last frame acceleration for body0
		scaledLFA = XMLoadFloat3(&mBodies[0]->GetLastFrameAcceleration()) * dt;

		velocityFromAcc += XMVector3Dot(scaledLFA, XMLoadFloat3(&mContactNormal)).m128_f32[0];
	}

	//Check if body1 is awake
	if (mBodies[1] && mBodies[1]->GetAwake()) {
		//Scale last frame acceleration for body1
		scaledLFA = XMLoadFloat3(&mBodies[1]->GetLastFrameAcceleration()) * dt;

		velocityFromAcc -= XMVector3Dot(scaledLFA, XMLoadFloat3(&mContactNormal)).m128_f32[0];
	}

	//Discourage vibration with resting contacts by manually lowering the coefficient of restitution.
	float thisRestitution = mRestitution;
	const float speed = XMVector3Length(XMLoadFloat3(&mContactVelocity)).m128_f32[0];
	if (speed < velocityLimit) {
		thisRestitution = mRestitution * (speed / velocityLimit);
	}

	mDesiredDeltaVelocity = -mContactVelocity.x - thisRestitution * (mContactVelocity.x - velocityFromAcc);
}

void Contact::CalculateInternals(float dt) {
	//Check if body0 is NULL
	if (!mBodies[0]) {
		//If NULL swap bodies
		SwapBodies();
	}assert(mBodies[0]);

	//Calculate axes at contact point
	CalculateContactBasis();

	//Store relative contact position to body0
	const XMVECTOR relativePosition0 = XMLoadFloat3(&mContactPoint) - XMLoadFloat3(&mBodies[0]->GetPosition());
	XMStoreFloat3(&mRelativeContactPosition[0], relativePosition0);

	//Check if body1 is NULL
	if (mBodies[1]) {
		//Store relative contact position to body0
		const XMVECTOR relativePosition1 = XMLoadFloat3(&mContactPoint) - XMLoadFloat3(&mBodies[1]->GetPosition());
		XMStoreFloat3(&mRelativeContactPosition[1], relativePosition1);
	}

	//Find the relative velocity of the bodies at the contact point
	mContactVelocity = CalculateLocalVelocity(0, dt);
	//Check if body1 is NULL
	if (mBodies[1]) {
		//Store relative contact position to body1
		const XMVECTOR contactVel1 = XMLoadFloat3(&mContactVelocity) - XMLoadFloat3(&CalculateLocalVelocity(1, dt));
		XMStoreFloat3(&mContactVelocity, contactVel1);
	}

	//Calculate the desired change in velocity
	CalculateDesiredDeltaVelocity(dt);
}

void Contact::ApplyVelocityChange(XMFLOAT3 velocityChange[2], XMFLOAT3 rotationChange[2]) {
	XMFLOAT3X3 inverseInertiaTensor[2];

	//Get the inverse mass and inverse inertia tensor in world coords
	inverseInertiaTensor[0] = mBodies[0]->GetInertiaTensorWorld();
	if (mBodies[1]) {
		inverseInertiaTensor[1] = mBodies[1]->GetInertiaTensorWorld();
	}

	//Impulse will be calculated for each contact axis
	XMFLOAT3 impulseContact;

	if (mFriction == 0.0f) {
		//Use frictionless impulse calculation
		impulseContact = CalculateFrictionlessImpulse(inverseInertiaTensor);
	}
	else {
		//Use friction impulse calculation
		impulseContact = CalculateFrictionImpulse(inverseInertiaTensor);
	}

	//Convert impulse to world coords
	const XMVECTOR xmImpulse = XMVector3Transform(XMLoadFloat3(&impulseContact), XMMatrixTranspose(XMLoadFloat3x3(&mContactToWorld))); 

	//Split the impulse into linear and rotational components
	const XMVECTOR xmImpulseTorque0 = XMVector3Cross(XMLoadFloat3(&mRelativeContactPosition[0]), xmImpulse);
	XMStoreFloat3(&rotationChange[0], XMVector3Transform(xmImpulseTorque0, XMLoadFloat3x3(&inverseInertiaTensor[0]))); 
	const XMVECTOR xmScaledImpulse0 = xmImpulse * mBodies[0]->GetInverseMass();
	const XMVECTOR xmVelocityChange0 = XMVectorZero();
	XMStoreFloat3(&velocityChange[0],xmVelocityChange0 + xmScaledImpulse0);

	//Apply changes
	mBodies[0]->AddVelocity(velocityChange[0]);
	mBodies[0]->AddRotation(rotationChange[0]);

	if (mBodies[1]) {
		//Split the impulse into linear and rotational components
		const XMVECTOR xmImpulseTorque1 = XMVector3Cross(xmImpulse, XMLoadFloat3(&mRelativeContactPosition[1]));
		XMStoreFloat3(&rotationChange[1], XMVector3Transform(xmImpulseTorque1, XMLoadFloat3x3(&inverseInertiaTensor[1])));
		const XMVECTOR xmScaledImpulse1 = xmImpulse * -mBodies[1]->GetInverseMass();
		const XMVECTOR xmVelocityChange1 = XMVectorZero();
		XMStoreFloat3(&velocityChange[1], xmVelocityChange1 + xmScaledImpulse1);

		//Apply changes
		mBodies[1]->AddVelocity(velocityChange[1]);
		mBodies[1]->AddRotation(rotationChange[1]);
	}
}

XMFLOAT3 Contact::CalculateFrictionlessImpulse(XMFLOAT3X3* inverseInertiaTensor) {
	XMVECTOR xmImpulseContact = XMVectorZero();

	//Calculate a vector that shows the change in velocity in world coordinates for a unit impulse in the direction of
	//the contact normal
	XMVECTOR xmDeltaVelWorld = XMVector3Cross(XMLoadFloat3(&mRelativeContactPosition[0]), XMLoadFloat3(&mContactNormal));
	xmDeltaVelWorld = XMVector3Transform(xmDeltaVelWorld, XMLoadFloat3x3(&inverseInertiaTensor[0])); 
	xmDeltaVelWorld = XMVector3Cross(xmDeltaVelWorld, XMLoadFloat3(&mRelativeContactPosition[0]));

	//Calculate the change in velocity in contact coords
	float deltaVelocity = XMVector3Dot(xmDeltaVelWorld, XMLoadFloat3(&mContactNormal)).m128_f32[0];

	//Add linear component of velocity change
	deltaVelocity += mBodies[0]->GetInverseMass();

	//Check if body1 is NULL
	if (mBodies[1]) {
		//Calculate a vector that shows the change in velocity in world coordinates for a unit impulse in the direction of
		//the contact normal
		xmDeltaVelWorld = XMVector3Cross(XMLoadFloat3(&mRelativeContactPosition[1]), XMLoadFloat3(&mContactNormal));
		xmDeltaVelWorld = XMVector3Transform(xmDeltaVelWorld, XMLoadFloat3x3(&inverseInertiaTensor[1])); 
		xmDeltaVelWorld = XMVector3Cross(xmDeltaVelWorld, XMLoadFloat3(&mRelativeContactPosition[1]));

		//Calculate the change in velocity in contact coords
		deltaVelocity = XMVector3Dot(xmDeltaVelWorld, XMLoadFloat3(&mContactNormal)).m128_f32[0];

		//Add linear component of velocity change
		deltaVelocity += mBodies[1]->GetInverseMass();
	}

	//Calculate the required size of the impulse
	const XMFLOAT3 impulseContact(mDesiredDeltaVelocity / deltaVelocity, 0.0f, 0.0f);

	return impulseContact;
}

XMFLOAT3 Contact::CalculateFrictionImpulse(XMFLOAT3X3* inverseInertiaTensor) {
	float inverseMass = mBodies[0]->GetInverseMass();

	//Create the skew symmetric / transformation turning force into torque for body 0.
	XMFLOAT3X3 impulseToTorque;
	impulseToTorque._11 = impulseToTorque._22 = impulseToTorque._33 = 0.0f;
	impulseToTorque._12 = -mRelativeContactPosition[0].z;
	impulseToTorque._13 = mRelativeContactPosition[0].y;
	impulseToTorque._21 = mRelativeContactPosition[0].z;
	impulseToTorque._23 = -mRelativeContactPosition[0].x;
	impulseToTorque._31 = -mRelativeContactPosition[0].y;
	impulseToTorque._32 = mRelativeContactPosition[0].x;

	const XMMATRIX xmInverseTensor0 = XMLoadFloat3x3(&inverseInertiaTensor[0]);

	//Calculate velocity change matrix for body 0.
	XMMATRIX xmDeltaVelWorld = XMLoadFloat3x3(&impulseToTorque);
	xmDeltaVelWorld *= xmInverseTensor0;
	xmDeltaVelWorld *= XMLoadFloat3x3(&impulseToTorque);
	xmDeltaVelWorld *= -1.0f;

	if (mBodies[1]) {
		//Create the skew symmetric / transformation turning force into torque for body 1.
		impulseToTorque._11 = impulseToTorque._22 = impulseToTorque._33 = 0.0f;
		impulseToTorque._12 = -mRelativeContactPosition[1].z;
		impulseToTorque._13 = mRelativeContactPosition[1].y;
		impulseToTorque._21 = mRelativeContactPosition[1].z;
		impulseToTorque._23 = -mRelativeContactPosition[1].x;
		impulseToTorque._31 = -mRelativeContactPosition[1].y;
		impulseToTorque._32 = mRelativeContactPosition[1].x;

		const XMMATRIX xmInverseTensor1 = XMLoadFloat3x3(&inverseInertiaTensor[1]);

		//Calculate velocity change matrix for body 1.
		XMMATRIX xmDeltaVelWorld2 = XMLoadFloat3x3(&impulseToTorque);
		xmDeltaVelWorld2 *= xmInverseTensor1;
		xmDeltaVelWorld2 *= XMLoadFloat3x3(&impulseToTorque);
		xmDeltaVelWorld2 *= -1.0f;

		//Add to total delta velocity.
		xmDeltaVelWorld += xmDeltaVelWorld2;

		inverseMass += mBodies[1]->GetInverseMass();
	}

	//Convert into contact coordinates.
	const XMMATRIX xmContactToWorld = XMLoadFloat3x3(&mContactToWorld);
	XMMATRIX xmDeltaVelocity = XMMatrixTranspose(xmContactToWorld);
	xmDeltaVelocity *= xmDeltaVelWorld;
	xmDeltaVelocity *= xmContactToWorld;

	//Add linear by adding the inverse mass to diagonals.
	XMFLOAT3X3 deltaVelocity;
	XMStoreFloat3x3(&deltaVelocity, xmDeltaVelocity);
	deltaVelocity._11 += inverseMass;
	deltaVelocity._22 += inverseMass;
	deltaVelocity._33 += inverseMass;
	xmDeltaVelocity = XMLoadFloat3x3(&deltaVelocity);

	//Invert to get the impulse needed per unit velocity.
	XMFLOAT3X3 impulseMatrix;
	XMStoreFloat3x3(&impulseMatrix, XMMatrixInverse(nullptr, xmDeltaVelocity));

	//Find the target velocities to kill.
	const XMFLOAT3 velKill(mDesiredDeltaVelocity, -mContactVelocity.y, -mContactVelocity.z);

	//Find the impulse to kill the target velocities.
	XMFLOAT3 impulseContact;
	XMStoreFloat3(&impulseContact, XMVector3Transform(XMLoadFloat3(&velKill), XMLoadFloat3x3(&impulseMatrix)));

	const float planarImpulse = sqrtf(impulseContact.y * impulseContact.y + impulseContact.z * impulseContact.z);
	//Check if we are within the limit of static friction.
	if (planarImpulse > impulseContact.x * mFriction) {
		//Handle dynamic friction.
		impulseContact.y /= planarImpulse;
		impulseContact.z /= planarImpulse;

		impulseContact.x = deltaVelocity._11 +
			deltaVelocity._12 * mFriction * impulseContact.y +
			deltaVelocity._13 * mFriction * impulseContact.z;
		impulseContact.x = mDesiredDeltaVelocity / impulseContact.x;
		impulseContact.y *= mFriction * impulseContact.x;
		impulseContact.z *= mFriction * impulseContact.x;
	}

	return impulseContact;
}

void Contact::ApplyPositionChange(XMFLOAT3 linearChange[2], XMFLOAT3 angularChange[2], float penetration) {
	const float angularLimit = 0.2f;
	float angularMove[2];
	float linearMove[2];
	float totalInertia = 0.0f;
	float linearInertia[2];
	float angularInertia[2];

	//Calculate the inertia of each body in the direction of the contact normal
	for (unsigned int i = 0; i < 2; i++) {
		if (!mBodies[i]->HasInfiniteMass()) {
			//Calculate the angular inertia
			XMVECTOR xmAngularInertiaWorld = XMVector3Cross(XMLoadFloat3(&mRelativeContactPosition[i]), XMLoadFloat3(&mContactNormal));
			xmAngularInertiaWorld = XMVector3Transform(xmAngularInertiaWorld, XMLoadFloat3x3(&mBodies[i]->GetInertiaTensorWorld())); 
			xmAngularInertiaWorld = XMVector3Cross(xmAngularInertiaWorld, XMLoadFloat3(&mRelativeContactPosition[i]));
			angularInertia[i] = XMVector3Dot(xmAngularInertiaWorld, XMLoadFloat3(&mContactNormal)).m128_f32[0];

			//Linear component is equal to the inverse mass
			linearInertia[i] = mBodies[i]->GetInverseMass();

			//Accumulate total inertia
			totalInertia += linearInertia[i] + angularInertia[i];
		}
	}

	//Calculate and apply changes
	for (unsigned int i = 0; i < 2; i++) {
		if (!mBodies[i]->HasInfiniteMass()) {
			const float sign = (i == 0) ? 1.0f : -1.0f;

			//Linear and angular movement are in proportion to the total inverse inertias
			angularMove[i] = sign * penetration * (angularInertia[i] / totalInertia);
			linearMove[i] = sign * penetration * (linearInertia[i] / totalInertia);

			//To avoid angular projections when mass is large and inertia tensor is small, limit the angular move
			const float relativeConPosDot = -XMVector3Dot(XMLoadFloat3(&mRelativeContactPosition[i]), XMLoadFloat3(&mContactNormal)).m128_f32[0];

			XMFLOAT3 projection;
			XMStoreFloat3(&projection, XMLoadFloat3(&mRelativeContactPosition[i]) + (XMLoadFloat3(&mContactNormal) * relativeConPosDot));

			//Approximate sine of the angle by using a small angle approximation
			const float maxMagnitude = angularLimit * sqrtf((projection.x * projection.x) + (projection.y * projection.y) + (projection.z * projection.z));

			if (angularMove[i] < -maxMagnitude) {
				const float totalMove = angularMove[i] + linearMove[i];
				angularMove[i] = -maxMagnitude;
				linearMove[i] = totalMove - angularMove[i];
			}
			else if (angularMove[i] > maxMagnitude) {
				const float totalMove = angularMove[i] + linearMove[i];
				angularMove[i] = maxMagnitude;
				linearMove[i] = totalMove - angularMove[i];
			}

			//Calculate the desired rotation to achieve required linear movement of bodies
			if (angularMove[i] == 0.0f) {
				//No angular movement
				angularChange[i] = XMFLOAT3(0.0f, 0.0f, 0.0f);
			}
			else {
				//Work out the direction of rotation
				XMStoreFloat3(&angularChange[i], XMVector3Transform(XMVector3Cross(XMLoadFloat3(&mRelativeContactPosition[i]), XMLoadFloat3(&mContactNormal)), XMLoadFloat3x3(&mBodies[i]->GetInertiaTensorWorld())) * (angularMove[i] / angularInertia[i]));
			}

			//Velocity change is the linear movement along the contact normal
			XMStoreFloat3(&linearChange[i], XMLoadFloat3(&mContactNormal) * linearMove[i]);

			//Apply linear movement
			XMFLOAT3 position;
			XMStoreFloat3(&position, XMLoadFloat3(&mBodies[i]->GetPosition()) + XMLoadFloat3(&mContactNormal) * linearMove[i]);
			mBodies[i]->SetPosition(position);

			//Apply change in orientation
			XMFLOAT4 orientation;
			XMStoreFloat4(&orientation, XMLoadFloat4(&mBodies[i]->GetOrientation()) + XMLoadFloat3(&angularChange[i]) * 1.0f);
			mBodies[i]->SetOrientation(orientation);

			//Calculate derived data in any body that is asleep so that the changes are reflected in the body's data.
			if (!mBodies[i]->GetAwake()) {
				mBodies[i]->CalculateDerivedData();
			}
		}
	}
}