#ifndef _CONTACTMANAGER_H_
#define _CONTACTMANAGER_H_

//Includes
#include "Contact.h"
#include "EngineEntity.h"
#include "CollisionPrimitive.h"
#include <unordered_map>

using namespace std;

class ContactManager
{
private:
	bool mValidSettings;
	unsigned int mVelocityIter;
	unsigned int mVelocityIterUsed;
	unsigned int mPositionIter;
	unsigned int mPositionIterUsed;
	float mVelocityEpsilon;
	float mPositionEpsilon;

	vector<unique_ptr<CollisionPrimitive>> mCollisionPrimitives;

public:
	ContactManager();
	~ContactManager();

	bool Initialize(unsigned int posIter, unsigned int velIter, float velocityEpsilon = 0.01f, float positionEpsilon = 0.01f);
	bool IsValid();
	void ResolveContacts(Contact* contacts, unsigned int numContacts, unsigned int maxContacts, float dt);
	void PrepareContacts(Contact* contacts, unsigned int numContacts, unsigned int maxContacts, float dt);
	void AdjustPositions(Contact* contacts, unsigned int numContacts, unsigned int maxContacts, float dt);
	void AdjustVelocities(Contact* contacts, unsigned int numContacts, unsigned int maxContacts, float dt);

	bool DoCollisionSystem(unsigned int maxContacts, float dt);

	vector<unique_ptr<CollisionPrimitive>>& GetCollisionPrimitives();
};

#endif