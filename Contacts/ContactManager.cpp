#include "ContactManager.h"
#include "CollisionDetector.h"
#include "CollisionCube.h"
#include "CollisionSphere.h"
#include "CollisionHalfSpace.h"

ContactManager::ContactManager(){
}

ContactManager::~ContactManager(){
}

bool ContactManager::Initialize(unsigned int posIter, unsigned int velIter, float velocityEpsilon, float positionEpsilon) {
	mPositionIter = posIter;
	mVelocityIter = velIter;
	mVelocityEpsilon = velocityEpsilon;
	mPositionEpsilon = positionEpsilon;
 
	return true;
}

bool ContactManager::IsValid() {
	return (mVelocityIter > 0) &&
		(mPositionIter > 0) &&
		(mPositionEpsilon >= 0.0f) &&
		(mVelocityEpsilon >= 0.0f);
}

void ContactManager::ResolveContacts(Contact* contacts, unsigned int numContacts, unsigned int maxContacts, float dt) {
	//Return if there are no contacts
	if (numContacts == 0) {
		return;
	}

	//Return if position and velocity iterations have not been set
	if (!IsValid()) {
		return;
	}

	//Prepare the contacts for processing
  	PrepareContacts(contacts, numContacts, maxContacts, dt);
	//Resolve interpenetrations with contacts
	AdjustPositions(contacts, numContacts, maxContacts, dt);
	//Resolve velocity changes with contacts
	AdjustVelocities(contacts, numContacts, maxContacts, dt);
}

void ContactManager::PrepareContacts(Contact* contacts, unsigned int numContacts, unsigned int maxContacts, float dt) {
	//Generate contact data i.e.(inertia, basis, etc.)
	for (unsigned int i = 0; i < numContacts; i++) {
		contacts[i].CalculateInternals(dt);
	}
}

void ContactManager::AdjustPositions(Contact* contacts, unsigned int numContacts, unsigned int maxContacts, float dt) {
	XMFLOAT3 linearChange[2];
	XMFLOAT3 angularChange[2];
	XMVECTOR xmDeltaPosition;

	//Iteratively resolve interpenetrations in order of the highest penetration first
	mPositionIterUsed = 0;
	while (mPositionIterUsed < mPositionIter) {
		float max = mPositionEpsilon;
		unsigned int index = numContacts;
		//Find the greatest penetration
		for (unsigned int i = 0; i < numContacts; i++) {
			if (contacts[i].mPenetration > max) {
				max = contacts[i].mPenetration;
				index = i;
			}
		}

		if (index == numContacts) {
			break;
		}

		//Match awake states
		contacts[index].MatchAwakeState();

		//Resolve penetration
		contacts[index].ApplyPositionChange(linearChange, angularChange, max);

		//Update contacts because the penetration fix may have changed the penetration of other bodies
		for (unsigned int i = 0; i < numContacts; i++) {
			//Check each body in contact
			for (unsigned int b = 0; b < 2; b++) if(contacts[i].mBodies[b]) {
				for (unsigned int d = 0; d < 2; d++) {
					//Check for a match with each body involved in the contact
					if (contacts[i].mBodies[b] == contacts[index].mBodies[d]) {
						xmDeltaPosition = XMLoadFloat3(&linearChange[d]) + XMVector3Cross(XMLoadFloat3(&angularChange[d]), XMLoadFloat3(&contacts[i].mRelativeContactPosition[b]));

						//Sign is negative if we are dealing with the first body in the contact and negative if we are dealing with the second body
						contacts[i].mPenetration += XMVector3Dot(xmDeltaPosition, XMLoadFloat3(&contacts[i].mContactNormal)).m128_f32[0] * (b ? 1.0f : -1.0f);
					}
				}
			}
		}
		//Update the postition iterations used
		mPositionIterUsed++;
	}
}

void ContactManager::AdjustVelocities(Contact* contacts, unsigned int numContacts, unsigned int maxContacts, float dt) {
	XMFLOAT3 velocityChange[2];
	XMFLOAT3 rotationChange[2];
	XMVECTOR xmDeltaVel;

	//Iteratively resolve delta velocity in order of the highest velocity change first
	mVelocityIterUsed = 0;
	while (mVelocityIterUsed < mVelocityIter) {
		float max = mVelocityEpsilon;
		unsigned int index = numContacts;
		//Find the contact with the highest probable velocity change
		for (unsigned int i = 0; i < numContacts; i++) {
			if (contacts[i].GetDesiredDeltaVelocity() > max) {
				max = contacts[i].GetDesiredDeltaVelocity();
				index = i;
			}
		}

		if (index == numContacts) {
			break;
		}

		//Match awake states
		contacts[index].MatchAwakeState();

		//Resolve delta velocity
		contacts[index].ApplyVelocityChange(velocityChange, rotationChange);

		//Update relative closing velocities because the change in velocity from the resolved contact
		for (unsigned int i = 0; i < numContacts; i++) {
			//Check each body in contact
			for (unsigned int b = 0; b < 2; b++) if (contacts[i].mBodies[b]){
				for (unsigned int d = 0; d < 2; d++) {
					//Check for a match with each body involved in the contact
					if (contacts[i].mBodies[b] == contacts[index].mBodies[d]) {
						xmDeltaVel = XMLoadFloat3(&velocityChange[d]) + XMVector3Cross(XMLoadFloat3(&rotationChange[d]), XMLoadFloat3(&contacts[i].mRelativeContactPosition[b]));

						//Sign is negative if we are dealing with the first body in the contact and negative if we are dealing with the second body
						XMStoreFloat3(&contacts[i].mContactVelocity, XMLoadFloat3(&contacts[i].mContactVelocity) + XMVector3Transform(xmDeltaVel, XMMatrixTranspose(XMLoadFloat3x3(&contacts[i].mContactToWorld))) * (b ? -1.0f : 1.0f));

						contacts[i].CalculateDesiredDeltaVelocity(dt);
					}
				}
			}
		}
		//Update the velocity iterations used
		mVelocityIterUsed++;
	}
}

bool ContactManager::DoCollisionSystem(unsigned int maxContacts, float dt) {
	CollisionData frameCollisionData;
	frameCollisionData.Reset(maxContacts);

	//Check each collision primitive
	for (auto entity = mCollisionPrimitives.begin(); entity != mCollisionPrimitives.end(); entity++) {
		entity->get()->CalculateInternals();

		//Check against every other collision primitive
		for (auto other = entity + 1; other != mCollisionPrimitives.end(); other++) {
			//Get bodies
			const PhysicsComponent* body[2] = {
				(*entity)->mBody,
				(*other)->mBody
			};

			//Check if each body NULL & if they have infinite mass
			bool bodystatic[2] = {
				body[0] && (body[0]->GetInverseMass() == 0),
				body[1] && (body[1]->GetInverseMass() == 0)
			};

			//Ignore invalid pairs
			if ((bodystatic[0] || (body[0] && !body[0]->GetAwake())) && (bodystatic[1] || (body[1] && !body[1]->GetAwake()))) {
				continue;
			}

			//Calculate transform matrix
			other->get()->CalculateInternals();

			//Check for sphere vs sphere collision
			if (dynamic_cast<CollisionSphere*>((*entity).get()) && dynamic_cast<CollisionSphere*>((*other).get())) {
				CollisionDetector::SphereAndSphere(*static_cast<CollisionSphere*>((*entity).get()), *static_cast<CollisionSphere*>((*other).get()), &frameCollisionData);
			}
			//Check for sphere vs cube collision
			else if (dynamic_cast<CollisionSphere*>((*entity).get()) && dynamic_cast<CollisionCube*>((*other).get())) {
				CollisionDetector::SphereAndCube(*static_cast<CollisionSphere*>((*entity).get()), *static_cast<CollisionCube*>((*other).get()), &frameCollisionData);
			}
			//Check for cube vs sphere collision
			else if (dynamic_cast<CollisionCube*>((*entity).get()) && dynamic_cast<CollisionSphere*>((*other).get())) {
				CollisionDetector::SphereAndCube(*static_cast<CollisionSphere*>((*other).get()), *static_cast<CollisionCube*>((*entity).get()), &frameCollisionData);
			}
			//Check for cube vs cube collision
			else if (dynamic_cast<CollisionCube*>((*entity).get()) && dynamic_cast<CollisionCube*>((*other).get())) {
				CollisionDetector::CubeAndCube(*static_cast<CollisionCube*>((*entity).get()), *static_cast<CollisionCube*>((*other).get()), &frameCollisionData);
			}
		}
	}

	if (frameCollisionData.mContactCount == 0) {
		return false;
	}


	//Check for contact clusters
	//Check each contact
	for (unsigned int i = 0; i < frameCollisionData.mContactCount; i++) {
		auto contact = frameCollisionData.mContacts[i];
		CollisionData frameClusterData;
		frameClusterData.Reset(maxContacts);
		Contact* cont = frameClusterData.AddContact();
		*cont = contact;

		//Check against other contacts
		for (unsigned int j = i + 1; j < frameCollisionData.mContactCount; j++) {

			//Get physics bodies for contact
			auto other = frameCollisionData.mContacts[j];
			const PhysicsComponent* body[2] = {
				contact.mBodies[0],
				contact.mBodies[1]
			};

			//Get physics bodies for other contact
			const PhysicsComponent* otherBody[2] = {
				other.mBodies[0],
				other.mBodies[1]
			};
			
			//Check that each body in first contact is or is not static
			bool bodystatic[2] = {
				body[0] && (body[0]->GetInverseMass() == 0),
				body[1] && (body[1]->GetInverseMass() == 0)
			};

			//Check that body0 is not static
			if (!bodystatic[0]) {
				//Check that other body0 and other body1 is not NULL & if either contact body matches other bodies
				if (otherBody[0] && body[0] == otherBody[0] || otherBody[1] && body[0] == otherBody[1]) {
					//Add contact to cluster
					Contact* oth = frameClusterData.AddContact();
					*oth = other;
				}
			}

			//Check that body1 is not static
			if (!bodystatic[1]) {
				//Check that other body0 and other body1 is not NULL & if either contact body matches other bodies
				if (otherBody[0] && body[1] == otherBody[0] || otherBody[1] && body[1] == otherBody[1]) {
					//Add contact to cluster
					Contact* oth = frameClusterData.AddContact();
					*oth = other;
				}
			}
		}

		//Resolve cluster
		ResolveContacts(frameClusterData.mContacts.data(), frameClusterData.mContactCount, maxContacts, dt);
	}
	
	return true;
}

vector<unique_ptr<CollisionPrimitive>>& ContactManager::GetCollisionPrimitives() {
	return mCollisionPrimitives;
}