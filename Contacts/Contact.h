#ifndef _CONTACT_H_
#define _CONTACT_H_

//Includes
#include "PhysicsComponent.h"

class Contact
{
public:
	float mPenetration;
	XMFLOAT3 mRelativeContactPosition[2];
	PhysicsComponent* mBodies[2];
	XMFLOAT3 mContactVelocity;
	XMFLOAT3X3 mContactToWorld;
	XMFLOAT3 mContactNormal;
	XMFLOAT3 mContactPoint;
	float mFriction;
	float mRestitution;
	float mDesiredDeltaVelocity;
	
public:
	Contact();
	~Contact();

	float GetDesiredDeltaVelocity();

	void SetBodiesData(PhysicsComponent* bodyOne, PhysicsComponent* bodyTwo);
	void MatchAwakeState();
	void SwapBodies();
	void CalculateContactBasis();
	XMFLOAT3 CalculateLocalVelocity(unsigned int bodyIndex, float dt);
	void CalculateDesiredDeltaVelocity(float dt);
	void CalculateInternals(float dt);
	void ApplyVelocityChange(XMFLOAT3 velocityChange[2], XMFLOAT3 rotationChange[2]);
	XMFLOAT3 CalculateFrictionlessImpulse(XMFLOAT3X3* inverseInertiaTensor);
	XMFLOAT3 CalculateFrictionImpulse(XMFLOAT3X3* inverseInertiaTensor);
	void ApplyPositionChange(XMFLOAT3 linearChange[2], XMFLOAT3 angularChange[2], float penetration);
};

#endif