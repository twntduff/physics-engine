#include "pOOBB.h"

pOOBB::pOOBB(){

}

pOOBB::~pOOBB(){

}

bool pOOBB::Initialize(const float posX, const float posY, const float posZ, const float yaw, const float pitch, const float roll, const float angle,
	const float angularDaming, const float linearDamping, const float density, const float restitution, const float friction,
	const float lengthX, const float lengthY, const float lengthZ) {
	if (!PhysicsComponent::Initialize(posX, posY, posZ, yaw, pitch, roll, angle, angularDaming, linearDamping, density, restitution, friction)) {
		return false;
	}

	mObjectType = Cube;

	mScale = XMFLOAT3(lengthX, lengthY, lengthZ);

	float mass = 0.0f;
	XMFLOAT3X3 inertiaTensor;
	XMMATRIX xmInertiaTensor;	
	XMMATRIX xmInverseTensor;

	// calculate mass based on density
	if (density > 0.0f) {
		const float volume = lengthX * lengthY * lengthZ;
		mass = volume * density;
	}
	
	inertiaTensor._11 = (0.0833f * mass) * (mScale.y + mScale.z);
	inertiaTensor._12 = 0.0f;
	inertiaTensor._13 = 0.0f;

	inertiaTensor._21 = 0.0f;
	inertiaTensor._22 = (0.0833f * mass) * (mScale.x + mScale.z);
	inertiaTensor._23 = 0.0f;

	inertiaTensor._31 = 0.0f;
	inertiaTensor._32 = 0.0f;
	inertiaTensor._33 = (0.0833f * mass) * (mScale.x + mScale.y);

	if (mass > 0.0f) {
		xmInertiaTensor = XMLoadFloat3x3(&inertiaTensor);
		xmInverseTensor = XMMatrixInverse(nullptr, xmInertiaTensor);
		XMStoreFloat3x3(&mInverseInertiaTensor, xmInverseTensor);
	}
	else {
		inertiaTensor._33 = 0.0f;
		xmInertiaTensor = XMLoadFloat3x3(&inertiaTensor);
		XMStoreFloat3x3(&mInverseInertiaTensor, xmInertiaTensor);
	}

	CalculateTransformMatrix();

	TransformInertiaTensor();

	return true;
}