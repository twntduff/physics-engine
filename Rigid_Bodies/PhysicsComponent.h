#ifndef _PHYSICSCOMPONENT_H_
#define _PHYSICSCOMPONENT_H_

//Includes
#include <memory>
#include <DirectXMath.h>

using namespace DirectX;
using namespace std;

enum ObjectType {
	Cube,
	Sphere
};

class PhysicsComponent
{
protected:
	bool mIsAwake;
	bool mCanSleep;
	float mInverseMass;
	float mLinearDamping;
	float mAngularDamping;
	float mMotion;
	float mFriction;
	float mRestitution;
	XMFLOAT4 mOrientation;
	XMFLOAT3 mVelocity;
	XMFLOAT3 mScale;
	XMFLOAT3 mAcceleration;
	XMFLOAT3 mLastFrameAcceleration;
	XMFLOAT3 mRotation;
	XMFLOAT3 mPosition;
	XMFLOAT3 mForceAccum;
	XMFLOAT3 mTorqueAccum;
	XMFLOAT4X4 mTransformMatrix;
	XMFLOAT3X3 mInverseInertiaTensor;
	XMFLOAT3X3 mInverseInertiaTensorWorld;
	ObjectType mObjectType;

public:
	PhysicsComponent();
	virtual ~PhysicsComponent();

	bool Initialize(const float posX, const float posY, const float posZ, const float yaw, const float pitch, const float roll, const float angle,
		const float angularDaming, const float linearDamping, const float density, const float restitution, const float friction);
	void Update(float dt);
	void CalculateDerivedData();
	void TransformInertiaTensor();
	void AddForce(const XMFLOAT3 force);
	void AddForceAtLocalPoint(const XMFLOAT3 force, const XMFLOAT3 point);
	void AddForceAtPoint(const XMFLOAT3 force, const XMFLOAT3 point);
	void ClearAccumulators();
	XMFLOAT3 GetPointInWorldSpace(XMFLOAT3 point);

	bool HasInfiniteMass();
	void SetAwake(const bool awake);
	void SetPosition(XMFLOAT3 position);
	void SetOrientation(XMFLOAT4 orientation);
	void AddVelocity(XMFLOAT3 deltaVelocity);
	void AddRotation(const XMFLOAT3& deltaRotation);

	bool GetAwake() const;
	float GetMass() const;
	float GetInverseMass() const;
	XMFLOAT3 GetRotation() const;
	XMFLOAT3 GetVelocity() const;
	XMFLOAT3 GetPosition() const;
	XMFLOAT4 GetOrientation() const;
	XMFLOAT3 GetLastFrameAcceleration() const;
	XMFLOAT3X3 GetInertiaTensorWorld() const;
	float GetFriction() const;
	float GetRestitution() const;
	ObjectType GetObjectType() const;

	void CalculateTransformMatrix();
	const XMFLOAT4X4& GetTransformMatrixAndRecalculate();
	const XMFLOAT4X4& GetTransformMatrix() const;
};

#endif