#include "PhysicsComponent.h"

PhysicsComponent::PhysicsComponent(){
	mCanSleep = true;
	mIsAwake = false;

	mMotion = 0.0f;
	mInverseMass = 0.0f;
	mLinearDamping = 0.0f;
	mAngularDamping = 0.0f;
	mRestitution = 0.0f;
	mFriction = 0.0f;
	
	XMStoreFloat3(&mRotation, XMVectorZero());
	XMStoreFloat3(&mPosition, XMVectorZero());
	XMStoreFloat3(&mLastFrameAcceleration, XMVectorZero());
	XMStoreFloat3(&mAcceleration, XMVectorZero());
	XMStoreFloat3(&mVelocity, XMVectorZero());
	XMStoreFloat3(&mForceAccum, XMVectorZero());
	XMStoreFloat3(&mTorqueAccum, XMVectorZero());

	XMStoreFloat4(&mOrientation, XMVectorZero());
	
	XMStoreFloat3x3(&mInverseInertiaTensor, XMMatrixIdentity());
	XMStoreFloat3x3(&mInverseInertiaTensorWorld, XMMatrixIdentity());
	XMStoreFloat4x4(&mTransformMatrix, XMMatrixIdentity());
}

PhysicsComponent::~PhysicsComponent(){
}

bool PhysicsComponent::Initialize(const float posX, const float posY, const float posZ, const float yaw, const float pitch, const float roll, const float angle,
	const float angularDaming, const float linearDamping, const float density, const float restitution, const float friction) {
	XMStoreFloat4(&mOrientation, XMVector4Normalize(XMQuaternionRotationNormal(XMVector3Normalize(XMLoadFloat3(&XMFLOAT3(yaw, pitch, roll))), XMConvertToRadians(angle))));

	mPosition.x = posX;
	mPosition.y = posY;
	mPosition.z = posZ;
	mAngularDamping = angularDaming;
	mLinearDamping = linearDamping;
	mInverseMass = 0.0f;
	
	mFriction = friction;
	mRestitution = restitution;
	
	if (density > 0.0f) {
		//Assume volume of 1
		mInverseMass = 1.0f / density; 
	}

	return true;
}

void PhysicsComponent::CalculateDerivedData() {
	XMStoreFloat4(&mOrientation, XMVector4Normalize(XMLoadFloat4(&mOrientation)));

	CalculateTransformMatrix();
	TransformInertiaTensor();
}

void PhysicsComponent::Update(float dt) {
	if (!mIsAwake) {
		return;
	}

	mLastFrameAcceleration = mAcceleration;

	XMStoreFloat3(&mLastFrameAcceleration, XMLoadFloat3(&mLastFrameAcceleration) + (XMLoadFloat3(&mForceAccum) * mInverseMass));
	XMStoreFloat3(&mVelocity, XMLoadFloat3(&mVelocity) + (XMLoadFloat3(&mLastFrameAcceleration) * dt) * powf(mLinearDamping, dt));
	XMStoreFloat3(&mRotation, XMLoadFloat3(&mRotation) + (XMVector3Transform(XMLoadFloat3(&mTorqueAccum), XMLoadFloat3x3(&mInverseInertiaTensorWorld)) * dt) * powf(mAngularDamping, dt));
	XMStoreFloat3(&mPosition, XMLoadFloat3(&mPosition) + (XMLoadFloat3(&mVelocity) * dt));

	XMFLOAT4 eularQuat(mRotation.x * dt, mRotation.y * dt, mRotation.z * dt, 1.0f);
	XMStoreFloat4(&eularQuat,XMQuaternionMultiply(XMLoadFloat4(&mOrientation), XMLoadFloat4(&eularQuat)) * 0.5f);
	XMStoreFloat4(&mOrientation,XMQuaternionNormalize(XMLoadFloat4(&eularQuat)));

	CalculateDerivedData();

	ClearAccumulators();

	if (mCanSleep) {
		const float currentMotion = XMVector3Dot(XMLoadFloat3(&mVelocity), XMLoadFloat3(&mVelocity)).m128_f32[0] + XMVector3Dot(XMLoadFloat3(&mRotation), XMLoadFloat3(&mRotation)).m128_f32[0];

		const float bias = powf(0.8f, dt);

		mMotion = bias * mMotion + (1.0f - bias) * currentMotion;

		const float sleepEpsilon = 0.0018f;

		if (mMotion < sleepEpsilon) {
			SetAwake(false);
		}
		else if (mMotion > 10.0f * sleepEpsilon) {
			mMotion = 10.0f * sleepEpsilon;
		}
	}
}

void PhysicsComponent::CalculateTransformMatrix() {
	const auto xmTransform = XMMatrixRotationQuaternion(XMLoadFloat4(&mOrientation)) * XMMatrixTranslationFromVector(XMLoadFloat3(&mPosition));

	XMStoreFloat4x4(&mTransformMatrix, xmTransform);
}

void PhysicsComponent::TransformInertiaTensor() {
	float t4 = mTransformMatrix._11 * mInverseInertiaTensor._11 +
		mTransformMatrix._12 * mInverseInertiaTensor._21 +
		mTransformMatrix._13 * mInverseInertiaTensor._31;

	float t9 = mTransformMatrix._11 * mInverseInertiaTensor._12 +
		mTransformMatrix._12 * mInverseInertiaTensor._22 +
		mTransformMatrix._13 * mInverseInertiaTensor._32;

	float t14 = mTransformMatrix._11 * mInverseInertiaTensor._13 +
		mTransformMatrix._12 * mInverseInertiaTensor._23 +
		mTransformMatrix._13 * mInverseInertiaTensor._33;

	float t28 = mTransformMatrix._21 * mInverseInertiaTensor._11 +
		mTransformMatrix._22 * mInverseInertiaTensor._21 +
		mTransformMatrix._23 * mInverseInertiaTensor._31;

	float t33 = mTransformMatrix._21 * mInverseInertiaTensor._12 +
		mTransformMatrix._22 * mInverseInertiaTensor._22 +
		mTransformMatrix._23 * mInverseInertiaTensor._32;

	float t38 = mTransformMatrix._21 * mInverseInertiaTensor._13 +
		mTransformMatrix._22 * mInverseInertiaTensor._23 +
		mTransformMatrix._23 * mInverseInertiaTensor._33;

	float t52 = mTransformMatrix._31 * mInverseInertiaTensor._11 +
		mTransformMatrix._32 * mInverseInertiaTensor._21 +
		mTransformMatrix._33 * mInverseInertiaTensor._31;

	float t57 = mTransformMatrix._31 * mInverseInertiaTensor._12 +
		mTransformMatrix._32 * mInverseInertiaTensor._22 +
		mTransformMatrix._33 * mInverseInertiaTensor._32;

	float t62 = mTransformMatrix._31 * mInverseInertiaTensor._13 +
		mTransformMatrix._32 * mInverseInertiaTensor._23 +
		mTransformMatrix._33 * mInverseInertiaTensor._33;

	mInverseInertiaTensorWorld._11 = t4 * mTransformMatrix._11 +
		t9 * mTransformMatrix._12 +
		t14 * mTransformMatrix._13;

	mInverseInertiaTensorWorld._12 = t4 * mTransformMatrix._21 +
		t9 * mTransformMatrix._22 +
		t14 * mTransformMatrix._23;

	mInverseInertiaTensorWorld._13 = t4 * mTransformMatrix._31 +
		t9 * mTransformMatrix._32 +
		t14 * mTransformMatrix._33;

	mInverseInertiaTensorWorld._21 = t28 * mTransformMatrix._11 +
		t33 * mTransformMatrix._12 +
		t38 * mTransformMatrix._13;

	mInverseInertiaTensorWorld._22 = t28 * mTransformMatrix._21 +
		t33 * mTransformMatrix._22 +
		t38 * mTransformMatrix._23;

	mInverseInertiaTensorWorld._23 = t28 * mTransformMatrix._31 +
		t33 * mTransformMatrix._32 +
		t38 * mTransformMatrix._33;

	mInverseInertiaTensorWorld._31 = t52 * mTransformMatrix._11 +
		t57 * mTransformMatrix._12 +
		t62 * mTransformMatrix._13;

	mInverseInertiaTensorWorld._32 = t52 * mTransformMatrix._21 +
		t57 * mTransformMatrix._22 +
		t62 * mTransformMatrix._23;

	mInverseInertiaTensorWorld._33 = t52 * mTransformMatrix._31 +
		t57 * mTransformMatrix._32 +
		t62 * mTransformMatrix._33;
}

void PhysicsComponent::AddForce(const XMFLOAT3 force) {
	XMStoreFloat3(&mForceAccum, XMLoadFloat3(&mForceAccum) + XMLoadFloat3(&force));

	mIsAwake = true;
}

void PhysicsComponent::ClearAccumulators() {
	XMStoreFloat3(&mForceAccum, XMVectorZero());
	XMStoreFloat3(&mTorqueAccum, XMVectorZero());
}

void PhysicsComponent::AddForceAtLocalPoint(const XMFLOAT3 force, const XMFLOAT3 point) {
	AddForceAtPoint(force, GetPointInWorldSpace(point));

	mIsAwake = true;
}

void PhysicsComponent::AddForceAtPoint(const XMFLOAT3 force, const XMFLOAT3 point) {
	XMStoreFloat3(&mForceAccum, XMLoadFloat3(&mForceAccum) + XMLoadFloat3(&force));

	XMStoreFloat3(&mTorqueAccum, XMLoadFloat3(&mTorqueAccum) + XMVector3Cross(XMLoadFloat3(&point) - XMLoadFloat3(&mPosition), XMLoadFloat3(&force)));

	mIsAwake = true;
}

XMFLOAT3 PhysicsComponent::GetPointInWorldSpace(XMFLOAT3 point) {
	XMFLOAT3 pt;
	XMStoreFloat3(&pt, XMVector3Transform(XMLoadFloat3(&point), XMLoadFloat4x4(&mTransformMatrix)));

	return pt;
}

void PhysicsComponent::AddVelocity(XMFLOAT3 deltaVelocity) {
	XMStoreFloat3(&mVelocity, XMLoadFloat3(&mVelocity) + XMLoadFloat3(&deltaVelocity));
}

void PhysicsComponent::AddRotation(const XMFLOAT3& deltaRotation) {
	XMStoreFloat3(&mRotation, XMLoadFloat3(&mRotation) + XMLoadFloat3(&deltaRotation));
}

void PhysicsComponent::SetPosition(XMFLOAT3 position) {
	mPosition = position;
}

void PhysicsComponent::SetOrientation(XMFLOAT4 orientation) {
	mOrientation = orientation;
}

bool PhysicsComponent::HasInfiniteMass() {
	if (mInverseMass <= 0.0f) {
		return true;
	}

	return false;
}

float PhysicsComponent::GetMass() const {
	if (mInverseMass == 0.0f) {
		return FLT_MAX;
	}
	else {
		return ((float)1.0f) / mInverseMass;
	}
}

float PhysicsComponent::GetInverseMass() const {
	return mInverseMass;
}

bool PhysicsComponent::GetAwake() const {
	return mIsAwake;
}

XMFLOAT3 PhysicsComponent::GetRotation() const {
	return mRotation;
}

XMFLOAT3 PhysicsComponent::GetVelocity() const {
	return mVelocity;
}

XMFLOAT3 PhysicsComponent::GetPosition() const {
	return mPosition;
}

XMFLOAT4 PhysicsComponent::GetOrientation() const {
	return mOrientation;
}

XMFLOAT3 PhysicsComponent::GetLastFrameAcceleration() const {
	return mLastFrameAcceleration;
}

XMFLOAT3X3 PhysicsComponent::GetInertiaTensorWorld() const {
	return mInverseInertiaTensorWorld;
}

float PhysicsComponent::GetFriction() const {
	return mFriction;
}

float PhysicsComponent::GetRestitution() const {
	return mRestitution;
}

ObjectType PhysicsComponent::GetObjectType() const {
	return mObjectType;
}

void PhysicsComponent::SetAwake(const bool awake) {
	if (awake) {
		mIsAwake = true;

		mMotion = 0.0018f * 2.0f;
	}
	else {
		mIsAwake = false;
		XMStoreFloat3(&mVelocity, XMVectorZero());
		XMStoreFloat3(&mRotation, XMVectorZero());
	}
}

const XMFLOAT4X4& PhysicsComponent::GetTransformMatrixAndRecalculate() {
	CalculateTransformMatrix();
	return GetTransformMatrix();
}

const XMFLOAT4X4& PhysicsComponent::GetTransformMatrix() const {
	return mTransformMatrix;
}

