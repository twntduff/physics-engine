#ifndef _PSPHERE_H_
#define _PSPHERE_H_

//Includes
#include "PhysicsComponent.h"

class pSphere : public PhysicsComponent
{
public:
	pSphere();
	~pSphere();

	bool Initialize(const float posX, const float posY, const float posZ, const float yaw, const float pitch, const float roll, const float angle, const float angularDaming, const float linearDamping, const float density,
		const float restitution, const float friction, const float radius);
};

#endif