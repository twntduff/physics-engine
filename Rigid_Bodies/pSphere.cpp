#include "pSphere.h"

pSphere::pSphere(){

}

pSphere::~pSphere(){

}

bool pSphere::Initialize(const float posX, const float posY, const float posZ, const float yaw, const float pitch, const float roll, const float angle,
	const float angularDaming, const float linearDamping, const float density, const float restitution, const float friction, const float radius) {
	if (!PhysicsComponent::Initialize(posX, posY, posZ, yaw, pitch, roll, angle, angularDaming, linearDamping, density, restitution, friction)) {
		return false;
	}

	mObjectType = Sphere;

	mScale = XMFLOAT3(radius, radius, radius);
	float mass = 0.0f;
	XMFLOAT3X3 inertiaTensor;
	XMMATRIX xmInertiaTensor;
	XMMATRIX xmInverseTensor;

	// calculate mass based on density
	if (density > 0) {
		const float volume = 4.0f / 3.0f * XM_PI * radius;//radii.x * radii.y * radii.z; for elipsis
		mass = volume * density;
	}	

	inertiaTensor._11 = 0.4f * mass * (radius * radius);
	inertiaTensor._12 = 0.0f;
	inertiaTensor._13 = 0.0f;

	inertiaTensor._21 = 0.0f;
	inertiaTensor._22 = 0.4f * mass * (radius * radius);
	inertiaTensor._23 = 0.0f;

	inertiaTensor._31 = 0.0f;
	inertiaTensor._32 = 0.0f;
	inertiaTensor._33 = 0.4f * mass * (radius * radius);

	if (mass > 0.0f) {
		xmInertiaTensor = XMLoadFloat3x3(&inertiaTensor);
		xmInverseTensor = XMMatrixInverse(nullptr, xmInertiaTensor);
		XMStoreFloat3x3(&mInverseInertiaTensor, xmInverseTensor);
	}
	else {
		inertiaTensor._33 = 1.0f;
		xmInertiaTensor = XMLoadFloat3x3(&inertiaTensor);
		XMStoreFloat3x3(&mInverseInertiaTensor, xmInertiaTensor);
	}

	CalculateTransformMatrix();

	TransformInertiaTensor();

	return true;
}