#include "EntityManager.h"
#include "gCube.h"
#include "gSphere.h"
#include "pOOBB.h"
#include "pSphere.h"
#include "CollisionCube.h"
#include "CollisionSphere.h"
#include "CollisionHalfSpace.h"


EntityManager::EntityManager() {
}

EntityManager::EntityManager(const EntityManager& other) {

}

EntityManager::~EntityManager() {
	Shutdown();
}

void EntityManager::Shutdown() {

}

bool EntityManager::Initialize() {
	mContactManager.reset(new ContactManager);
	mContactManager->Initialize(1, 1);

	mForceManager.reset(new ForceManager);

	return true;
}

unsigned int EntityManager::GetNextValidID() {
	if (mEntityHash.empty()) {
		return 0;
	}
	else {
		return (unsigned int)mEntityHash.size();
	}
}

EngineEntity* EntityManager::AddEntity(GraphicsComponent* graphicsComponent, PhysicsComponent* physicsComponent) {
	EngineEntity* entity = new EngineEntity;
	unique_ptr<EngineEntity> smrtEntity(entity);

	if (!entity) {
		return nullptr;
	}

	if (!entity->Initialize(graphicsComponent, physicsComponent)) {
		return nullptr;
	}

	smrtEntity->SetID(GetNextValidID());

	smrtEntity.swap(mEntityHash[entity->GetID()]);

	return entity;
}

EngineEntity* EntityManager::AddCube(ID3D11Device* device, const float red, const float green, const float blue, const float alpha,
	const float posX, const float posY, const float posZ, const float yaw, const float pitch, const float roll, const float angle,
	const float angularDaming, const float linearDamping, const float mass, const float restitution, const float friction,
	const float lengthX, const float lengthY, const float lengthZ) {

	gCube* graphicsComponent = new gCube;

	if (!graphicsComponent->InitializeBuffers(device, red, green, blue, alpha, XMFLOAT3(lengthX / 2.0f, lengthY / 2.0f, lengthZ / 2.0f))) {
		return nullptr;
	}

	pOOBB* physicsComponent = new pOOBB;

	if (!physicsComponent->Initialize(posX, posY, posZ, yaw, pitch, roll, angle, angularDaming, linearDamping, mass, restitution, friction, lengthX, lengthY, lengthZ)) {
		return nullptr;
	}

	CollisionCube* collCube = new CollisionCube;
	unique_ptr<CollisionPrimitive> upCollCube(collCube);
	collCube->mBody = physicsComponent;
	collCube->mHalfSize = XMFLOAT3(lengthX / 2.0f, lengthY / 2.0f, lengthZ / 2.0f);

	mContactManager->GetCollisionPrimitives().push_back(move(upCollCube));

	return AddEntity(graphicsComponent, physicsComponent);
}

EngineEntity* EntityManager::AddSphere(ID3D11Device* device, const float red, const float green, const float blue, const float alpha,
	const float posX, const float posY, const float posZ, const float yaw, const float pitch, const float roll, const float angle,
	const float angularDaming, const float linearDamping, const float mass, const float restitution, const float friction, const float radius) {

	gSphere* graphicsComponent = new gSphere;

	if (!graphicsComponent->InitializeBuffers(device, red, green, blue, alpha, radius)) {
		return nullptr;
	}

	pSphere* physicsComponent = new pSphere;

	if (!physicsComponent->Initialize(posX, posY, posZ, yaw, pitch, roll, angle, angularDaming, linearDamping, mass, restitution, friction, radius)) {
		return nullptr;
	}

	CollisionSphere* collSphere = new CollisionSphere;
	unique_ptr<CollisionPrimitive> upCollSphere(collSphere);
	collSphere->mBody = physicsComponent;
	collSphere->mRadius = radius;

	mContactManager->GetCollisionPrimitives().push_back(move(upCollSphere));

	return AddEntity(graphicsComponent, physicsComponent);
}

bool EntityManager::DeleteEntity(unsigned int id) {
	unordered_map<unsigned int, unique_ptr<EngineEntity>>::const_iterator found = mEntityHash.find(id);

	bool foundAtLeast1 = false;

	if (found != mEntityHash.end()) {
		mEntityHash.erase(found);
		foundAtLeast1 = true;
	}

	vector<unique_ptr<CollisionPrimitive>>::const_iterator it1 = mContactManager->GetCollisionPrimitives().begin();
	while (it1 != mContactManager->GetCollisionPrimitives().end()) {
		if (it1->get()->mBody == found->second.get()->GetPhysicsComponent().get()) {
			it1 = mContactManager->GetCollisionPrimitives().erase(it1);
		}
		else {
			it1++;
		}
	}

	vector<ForceDecl>::const_iterator it2 = mForceManager->GetForceDeclerations().begin();
	while (it2 != mForceManager->GetForceDeclerations().end()) {
		if (it2->Entity == found->second.get()->GetPhysicsComponent().get()) {
			it2 = mForceManager->GetForceDeclerations().erase(it2);
		}
		else {
			it2++;
		}
	}

	if (!foundAtLeast1) {
		return false;
	}

	return true;
}

void EntityManager::DeleteAllEntities() {
	unordered_map<unsigned int, unique_ptr<EngineEntity>>::const_iterator it1 = mEntityHash.begin();
	while (it1 != mEntityHash.end()) {
		it1 = mEntityHash.erase(it1);
	}

	mEntityHash.clear();

	vector<unique_ptr<CollisionPrimitive>>::const_iterator it2 = mContactManager->GetCollisionPrimitives().begin();
	while (it2 != mContactManager->GetCollisionPrimitives().end()) {
		it2 = mContactManager->GetCollisionPrimitives().erase(it2);
	}

	mContactManager->GetCollisionPrimitives().clear();

	vector<ForceDecl>::const_iterator it3 = mForceManager->GetForceDeclerations().begin();
	while (it3 != mForceManager->GetForceDeclerations().end()) {
		it3 = mForceManager->GetForceDeclerations().erase(it3);
	}

	mForceManager->GetForceDeclerations().clear();
}

unordered_map<unsigned int, unique_ptr<EngineEntity>>& EntityManager::GetEntityHash() {
	return mEntityHash;
}

unique_ptr<ForceManager>& EntityManager::GetForceManager() {
	return mForceManager;
}

unique_ptr<ContactManager>& EntityManager::GetContactManager() {
	return mContactManager;
}