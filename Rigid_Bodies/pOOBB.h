#ifndef _POOBB_H_
#define _POOBB_H_

//Includes
#include "PhysicsComponent.h"

class pOOBB : public PhysicsComponent
{
public:
	pOOBB();
	~pOOBB();

	bool Initialize(const float posX, const float posY, const float posZ, const float yaw, const float pitch, const float roll, const float angle,
		const float angularDaming, const float linearDamping, const float density, const float restitution, const float friction,
		const float lengthX, const float lengthY, const float lengthZ);
};

#endif