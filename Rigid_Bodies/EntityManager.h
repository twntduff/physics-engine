#ifndef _ENTITYMANAGER_H_
#define _ENTITYMANAGER_H_

//Includes
#include <unordered_map>
#include <memory>
#include "EngineEntity.h"
#include "ContactManager.h"
#include "ForceManager.h"

class gCube;
class gSphere;
class pOOBB;
class pSphere;
class gHalfSpace;
class pHalfSpace;

using namespace std;

class EntityManager
{
private:
	unordered_map<unsigned int, unique_ptr<EngineEntity>> mEntityHash;
	unique_ptr<ContactManager> mContactManager;
	unique_ptr<ForceManager> mForceManager;

public:
	EntityManager();
	EntityManager(const EntityManager& other);
	~EntityManager();

	void Shutdown();
	bool Initialize();
	unsigned int GetNextValidID();
	EngineEntity* AddEntity(GraphicsComponent* graphicsComponent, PhysicsComponent* physicsComponent);
	EngineEntity* AddCube(ID3D11Device* device, const float red, const float green, const float blue, const float alpha,
		const float posX, const float posY, const float posZ, const float yaw, const float pitch, const float roll, const float angle,
		const float angularDaming, const float linearDamping, const float mass, const float restitution, const float friction,
		const float lengthX, const float lengthY, const float lengthZ);
	EngineEntity* AddSphere(ID3D11Device* device, const float red, const float green, const float blue, const float alpha,
		const float posX, const float posY, const float posZ, const float yaw, const float pitch, const float roll, const float angle,
		const float angularDaming, const float linearDamping, const float mass, const float restitution, const float friction, const float radius);
	bool DeleteEntity(unsigned int id);
	void DeleteAllEntities();

	unordered_map<unsigned int, unique_ptr<EngineEntity>>& GetEntityHash();
	unique_ptr<ForceManager>& GetForceManager();
	unique_ptr<ContactManager>& GetContactManager();
};

#endif